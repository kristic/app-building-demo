# Build-A-Team Sample App

This app shell has everything you need to start building a Backbone-Marionette Single Page App.

### This App Shell is for Learning Purposes Only.

This is an extrodinarily simple back-end, designed to give students a very basic RESTful API to learn Javascript App Development. It is not prepared or secured for any kind of production use.

## Installation

The following instructions should be following using a terminal (command line), inside of the `buildateam` folder: 

* Make sure you have rails installed:   
`gem install rails`
* Install the gem dependencies with bundler:  
`bundle install`
* Install, Migrate and Seed your database (SQLite, included):  
`rake db:migrate && rake db:seed`
* Once complete, start the server with:  
`rails s`

## The Base HTML File

This app is configured so that any url not pointing to an API endpoint will direct to the homepage at `app/views/site/home`.

This allows you to use "real" URLs (without hashes) in your Backbone app: http://stackoverflow.com/a/7310505 (more information on this is in the course)

## Static File Libraries

This shell has all the libraries you need to start developing your app according to the workshop course, including:
- Backbone, Marionette (and assoc. requirements)
- Bootstrap

The `application.js` file is set up with these dependencies in the order you need them. This is of course just a starting point!

## API Endpoints

The following is a guide for you to know what to expect from each API URL endpoint. Each endpoint listed here allows a GET (aka, read) request, with other **"Verbs"** available as described in the **"Other Actions"** section below each type.

### Members

#### List Members
**URL**: `/api/members`

Seed Data: 
``` json
[
  {
    "id": 1,
    "first_name": "Person",
    "last_name": "One",
    "created_at": "2014-03-27T23:54:50.706Z",
    "updated_at": "2014-03-27T23:54:50.706Z"
  },
  {
    "id": 2,
    "first_name": "Person",
    "last_name": "Two",
    "created_at": "2014-03-27T23:54:50.714Z",
    "updated_at": "2014-03-27T23:54:50.714Z"
  },
 ...
]
```

#### Other Actions
* CREATE (post)

### Display a Member
**URL**: `api/members/:id` 

Seed Data:
``` json
  {
    "id": 1,
    "first_name": "Person",
    "last_name": "One",
    "created_at": "2014-03-27T23:54:50.706Z",
    "updated_at": "2014-03-27T23:54:50.706Z"
  }
```

#### Other Actions:
* DELETE 
* UPDATE (put)

### Member's Teams
**URL**: `api/members/:id/teams`

``` json 
[
  {
    "id": 1,
    "name": "Example Team One",
    "captain_id": null,
    "created_at": "2014-03-27T23:54:50.731Z",
    "updated_at": "2014-03-27T23:54:50.731Z",
    "category_id": null
  },
  {
    "id": 2,
    "name": "Example Team Two",
    "captain_id": null,
    "created_at": "2014-03-27T23:54:50.735Z",
    "updated_at": "2014-03-27T23:54:50.735Z",
    "category_id": null
  }
]
```

### Other Actions:
  * (none)

### Teams
**URL**: `api/teams`

``` json
[
  {
    "id": 1,
    "name": "Example Team One",
    "captain_id": null,
    "created_at": "2014-03-27T23:54:50.731Z",
    "updated_at": "2014-03-27T23:54:50.731Z",
    "category_id": null
  },
  {
    "id": 2,
    "name": "Example Team Two",
    "captain_id": null,
    "created_at": "2014-03-27T23:54:50.735Z",
    "updated_at": "2014-03-27T23:54:50.735Z",
    "category_id": null
  }
]
```

#### Other Actions:
* CREATE (post)

### Display A Team
**URL**: `api/teams/:id`

``` json
{
  "id": 1,
  "name": "Example Team One",
  "captain_id": null,
  "created_at": "2014-03-27T23:54:50.731Z",
  "updated_at": "2014-03-27T23:54:50.731Z",
  "category_id": null
}

```

#### Other Actions:
* UPDATE (put)
* DELETE

### Team's Members
**URL**: `api/teams/:id/members`

``` json
[
  {
    "id": 1,
    "first_name": "Person",
    "last_name": "One",
    "created_at": "2014-03-27T23:54:50.706Z",
    "updated_at": "2014-03-27T23:54:50.706Z"
  },
  {
    "id": 2,
    "first_name": "Person",
    "last_name": "Two",
    "created_at": "2014-03-27T23:54:50.714Z",
    "updated_at": "2014-03-27T23:54:50.714Z"
  },
  ...
]

```

#### Other Actions:
* (none)

### Categories
**URL**: `api/categories`

``` json
[
  {
    "id": 1,
    "name": "Category One",
    "parent_id": null,
    "created_at": "2014-03-27T23:54:50.763Z",
    "updated_at": "2014-03-27T23:54:50.763Z"
  },
  {
    "id": 2,
    "name": "Category Two",
    "parent_id": 1,
    "created_at": "2014-03-27T23:54:50.767Z",
    "updated_at": "2014-03-27T23:54:50.767Z"
  },
  ...
]

```

#### Other Actions:
* CREATE (post)

### Category
**URL**: `api/categories/:id`
``` json
{
  "category": {
    "id": 1,
    "name": "Category One",
    "parent_id": null,
    "created_at": "2014-03-27T23:54:50.763Z",
    "updated_at": "2014-03-27T23:54:50.763Z"
  }
}

```

#### Other Actions:
* DELETE
* UPDATE (put)

### Additional Options

** Get children along with category GET (read only): **
`api/categories/:id?children=true`

Results in:
```json

{
  "category": {
    "id": 1,
    "name": "Category One",
    "parent_id": null,
    "created_at": "2014-03-27T19:22:45.140Z",
    "updated_at": "2014-03-27T19:22:45.140Z"
  },
  "children": [
    {
      "id": 2,
      "name": "Category Two",
      "parent_id": 1,
      "created_at": "2014-03-27T19:22:45.144Z",
      "updated_at": "2014-03-27T19:22:45.144Z"
    }
  ]
}


```


### Sub-Categories
**URL**: `/api/categories/:id/subcategories`

```json
[
  {
    "id": 2,
    "name": "Category Two",
    "parent_id": 1,
    "created_at": "2014-03-27T19:22:45.144Z",
    "updated_at": "2014-03-27T19:22:45.144Z"
  }
]

```

### Other Actions:
* (none)


