# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Member.create([
    { first_name: "Person", last_name: "One" }, 
    { first_name: "Person", last_name: "Two" }, 
    { first_name: "Person", last_name: "Three" }, 
    { first_name: "Person", last_name: "Four" } 
  ])

Team.create([
		{ name: "Example Team One"},
		{ name: "Example Team Two"}
	])

MemberTeam.create([
		{ member_id: 1, team_id: 1 },
		{ member_id: 2, team_id: 1 },
		{ member_id: 3, team_id: 1 },
		{ member_id: 4, team_id: 2 },
		{ member_id: 1, team_id: 2 }
	])

Category.create([
    {name: "Category One"},
    {name: "Category Two", parent_id: 1},
    {name: "Category Three", parent_id: 2},
    {name: "Category Four"},
    {name: "Category Five", parent_id: 3}
  ])