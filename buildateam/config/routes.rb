Buildateam::Application.routes.draw do

  namespace :api, defaults: {format: :json}  do
    # api resources will go here
    resources :categories do
      resources :subcategories, module: :categories, only: :index
    end

    resources :members do
      resources :teams, module: :members, only: :index
    end

    resources :teams do
      resources :members, module: :teams, only: :index
    end

  end

  # module => 'site' tells rails to look for a folder inside views called 'site'
  # and anything inside this block will live inside that 'site' folder
  scope :module => 'site', defaults: {format: :html} do
    # our default route is pointing to a view 'home#index'
    # which tells rails to look for a file 'index.html' inside a folder 'home' (within 'site')
    root :to =>'home#index'
    # anything else also goes here unless specifically set in our routes (aka, a "splat")
    get '*path' => 'home#index'
  end

end
