class Member < ActiveRecord::Base
	has_many :member_teams
	has_many :teams, through: :member_teams
end
