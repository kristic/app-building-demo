class Team < ActiveRecord::Base
	has_many :member_teams
	has_many :members, through: :member_teams
  belongs_to :category
end
