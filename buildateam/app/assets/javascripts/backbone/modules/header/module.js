App.module("Header", function(Header, App, Backbone, Marionette, $, _) {



  Header.on('start', function() {
    App.execute('header:show');
  });

  App.addInitializer(function() {

    App.commands.setHandler('header:show', function() {
      new Header.Show.Controller({
        region: App.headerRegion
      });
    });

  });

});
