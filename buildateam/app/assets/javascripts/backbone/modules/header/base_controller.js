App.module("Header.Show", function(Show, App, Backbone, Marionette, $, _) {

  Show.Controller = Marionette.Controller.extend({
    initialize: function(options) {
      if(options.region) {
        this.showView(options.region);
      }
    },

    showView: function(region) {
      region.show(new Show.View());
    }
  });

});