App.module("Header.Show", function(Show, App, Backbone, Marionette, $, _) {

  Show.View = Marionette.ItemView.extend({
    template: 'header/show'
  });

});