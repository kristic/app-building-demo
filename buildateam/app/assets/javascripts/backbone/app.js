(function() {
  this.App = (function(Backbone, Marionette) {
    var App, Router, API;

    App = new Marionette.Application();

    App.reqres = new Backbone.Wreqr.RequestResponse();

    App.addRegions({
      headerRegion: '#header-region',
      mainRegion: '#main-region'
    });

    App.addInitializer(function() {
      console.log('App started');

      App.reqres.setHandler('test:request', function() {
        return "Request received, here's some data!";
      });
    });


    return App;
  
  })(Backbone, Marionette);

}).call(this);