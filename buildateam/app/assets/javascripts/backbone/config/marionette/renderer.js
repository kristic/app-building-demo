
_.extend(Marionette.Renderer, { 
    // the directory of all templates in the app
    lookups: ["backbone/templates/"],

    render: function(template, data) {
      var path = this.getTemplate(template);

      if(path) { 
        return path(data);
      } else {
         throw "Template "+ template +" not found!"; 
      }
    },

    getTemplate: function(template) {
      var jsTemplate,
          lookups = this.lookups;

      _.each([template], function(path) {
        for (var i = 0; i < lookups.length; i++) {
          var lookup = lookups[i];

          if(JST[lookup + path]) {
            jsTemplate = JST[lookup + path];
            break;
          }
        }
      });

      return jsTemplate;
    }

});