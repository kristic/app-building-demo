class Api::CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :update, :destroy]

  def index
    render json: Category.all
  end

  def show
    if params[:children] == 'true'
      render json: { category: @category, children: @category.children }    
    else
      render json: @category
    end
  end

  def create
    @category = ::Category.new(category_params)
    if @category.save
      render json: @category, status: :created
    else
     render json: @category.errors, status: :unprocessable_entity
    end 
  end

  def update
    if @category.update(category_params)
      render json: @category
    else
      render_error(@category)
    end
  end

  def destroy
    if @category.destroy
      head :no_content
    else
      head :unprocessable_entity
    end
  end

  def category_params
    params.permit(:name, :parent_id)
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_category
    @category = ::Category.find(params[:id])
  end

end
