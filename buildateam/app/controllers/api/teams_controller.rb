class Api::TeamsController < ApplicationController
  before_action :set_team, only: [:show, :update, :destroy]

  def show
    render json: @team
  end

  def index
    @teams = Team.all
    render json: @teams
  end

  def create
    @team = ::Team.new(team_params)
    if @team.save
      render json: @team, status: :created
    else
     render json: @team.errors, status: :unprocessable_entity
    end 
  end

  def update
    @team.update(team_params)
    unless @team.errors.any?
      render json: @team
    else
      render_error(@team)
    end
  end

  def destroy
    if @team.destroy
      head :no_content
    else
      head :unprocessable_entity
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def team_params
    params.permit(:name, :captain_id, :category_id)
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_team
    @team = ::Team.find(params[:id])
  end

end
