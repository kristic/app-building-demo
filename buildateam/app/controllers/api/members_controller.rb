class Api::MembersController < ApplicationController
  before_action :set_member, only: [:show, :update, :destroy]

  def show
    @member = Member.find(params[:id])
    render json: @member
  end

  def index
    @members = Member.all
    render json: @members
  end

  def update
    @member.update(member_params)
    unless @member.errors.any?
      render json: @member
    else
      render_error(@member)
    end
  end

  def create
    @member = ::Member.new(member_params)
    if @member.save
      render json: @member, status: :created
    else
     render json: @member.errors, status: :unprocessable_entity
    end
  end

  def destroy
    if @member.destroy
      head :no_content
    else
      head :unprocessable_entity
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def member_params
    params.permit(:first_name, :last_name)
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_member
    @member = ::Member.find(params[:id])
  end

end
