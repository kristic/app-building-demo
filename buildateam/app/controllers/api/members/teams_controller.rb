class Api::Members::TeamsController < ApplicationController

  def index
    @teams = Member.find(params[:member_id]).teams
    render json: @teams
  end
end
