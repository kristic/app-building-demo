class Api::Teams::MembersController < ApplicationController

  def index
    @members = Team.find(params[:team_id]).members
    render json: @members
  end

end
