class Api::Categories::SubcategoriesController < ApplicationController

  def index
    @subcategories = Category.find(params[:category_id]).children
    render json: @subcategories
  end

end
