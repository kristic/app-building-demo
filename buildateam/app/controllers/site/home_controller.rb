class Site::HomeController < ApplicationController
  def index
      # return a 404 if the format is anything except 'html'
     head :not_found unless request.format == 'text/html'
  end
end
