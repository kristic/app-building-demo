# App Requirement Ideas

1. Users & User Profiles
	* Use oAuth (Google Sign In)
2. List of Stuff!
3. Individual view of Stuff
4. User-specific list of stuff (add & remove)



http://designmodo.github.io/Flat-UI/

## Ideas

* Beer/Wine - Profiles, Reviews & Ratings 
	- http://www.brewerydb.com/developers/docs 
		- API KEY: e042647ddc7a8c1866533a9014e3e1f8
	- http://api.snooth.com/
		- API KEY: 4f4wzisddu0ohv37mj894csbvbnqcqlrzcu5i7sv8ayztfst
* Goodreads API - Book Listings & Faves
  - key: xG4px8ZYjHVObyWjiAMJ9A
  - secret: QBkp0RizxuwCW6DgM4QZhRiDCc7wwgJWLrTt4uIKE8
* Google Books API - https://developers.google.com/books/docs/v1/getting_started
* Open Library API - https://openlibrary.org/developers
