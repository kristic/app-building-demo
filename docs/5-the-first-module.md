# The First Module

Our API is ready and waiting for us, our application is ready to go, now let's tell this homepage to do something interesting.

The first thing we want to do is resolve what happens when a user visits the "homepage" of our app. This basically means what *route* is triggered when we go to `/`.

We'll also match that up with an **App Command**, which acts a whole lot like the `App.reqres` we wrote, but doesn't return a value.

Let's start by creating a module for the header, which will render in the `div#header-region`.

## Setting up the Module files

* Within the `assets/javascripts/backbone` folder, create a new folder called `modules`.

* Create a new folder `header` within `modules`.

* Create a new file `module.js` within `header`.

* Create a new folder `base` within `header` as well.

* Create 2 new files within `base`: `base_view.js` and `base_controller.js`.

When you're done, the folder structure should look something like this:

![folder structure](images/module-folders.png "folder structure")

*You can optionally drop the `base_view` and `base_controller ` files into a `base` folder. Up to you.*

Let's also add this new folder to the `application.js` file so it get picked up by rails.

``` javascript
//= require_tree ./backbone/modules
```

This time we're telling Rails to include the **entire** modules folder, after `app.js`.

Make sure you add this line **directly below** the last include comment inside this file.

## Build the Module JS File

Every one of our module, entity, and view files will be wrapped in the following module pattern, so you'll see the following pattern *a-lot*.


``` javascript
App.module("Header", function(Header, App, Backbone, Marionette, $, _) {
  
  //.. all kinds of code in here

});

```

`App.module` defines that this is a module inside of our app (called, "App", as we described it inside `app.js`).

`"Header"` will be the name of this module.

`function(Header, App, Backbone, Marionette, $, _) { ...`  This initializer function namespaces both the app, our new module "header" as well as the variables used by our core libraries Backbone, Marionette, jQuery, and underscore. Marionette adds these as arguments by default, in this order, into it's module callback function.


Next, we'll add some code that will be set up when the App initializes. We want to set an event handler that can be triggered, to show the Header module.

``` javascript

  App.addInitializer(function() {

    App.commands.setHandler('header:show', function() {
      // instantiate a new Header Show Controller
      // when this event is triggered
      new Header.Show.Controller({
        region: App.headerRegion
      });
    });
  });

```

When we execute a command called `'header:show'`, it will call this function which will create a new Header Controller.

We're also passion in a `region` option which we'll need to handle inside of the new Show Controller we're going to write very soon.

First though, since this is the header, we want it to show whenever the App is started, right away. By default, this module is *also* started with the app. So, we can write an `.on('start')` event handler to trigger when the app and module are loaded and started.

``` javascript
  Header.on('start', function() {
    App.execute('header:show');
  });

  App.addInitializer(function() {
    ...
```

Now, the Header Show Controller will be created when the App starts.

## Build the Header Show Controller

Just like the module, wrap the `show_controller.js` code inside an `App.module` method, but with one slight difference - the "Show" namespace: 

``` javascript
App.module("Header.Show", function(Show, App, Backbone, Marionette, $, _) {

  Show.Controller = Marionette.Controller.extend({
   // ... controller code here
  });

});
```

So by calling `App.module` with `"Header.Show"` instead of just `"Header"`, we created a new namespace inside of Header.

Also note the first argument in the callback function is `Show` - this is a reference to the namespace we just created. This doesn't have to be Show, but it's easiest to understand by using the name you just declared in the first argument.

Just like most Backbone Modules, when it's instantiated with a `new`, it triggers it's `initialize` function, which we'll write now for our new controller:

``` javascript
  Show.Controller = Marionette.Controller.extend({
    initialize: function(options) {
      if(options.region) {
        this.showView(options.region);
      }
    },

    showView: function(region) {
      region.show(new Show.View());
    }
  });
```

So, now, when this controller is called with `new`, it will reference the options passed to it, and see if one of them is called `region`. If so, it will send that region to it's own method called `showView`, where it will create a new instance of `Show.View` (we'll write that next), and render it within the region we specified.

That's it for the Controller for now.

## Build the Header Show View

The last piece is the Show View. Let's start the file by declaring it as part of the `Header.Show` module:

``` javascript
App.module("Header.Show", function(Show, App, Backbone, Marionette, $, _) {

  Show.View = Marionette.ItemView.extend({
    // ...
  });

});
```

The `ItemView` is one of Marionette's special kind of Backbone Views - but is also the simplest, and behaves mostly like a regular Backbone View, with a few exceptions. For one, with any Marionette View, when you create a `new` view and a region calls it with `show` like we did inside of the Show.Controller, it will automatically `render` itself into that region.

We need a few more configurations for our view before it will work though. All views need at least a `template` option set.

Because we haven't set up a **template renderer** yet, let's just put a simple string of HTML in here just to make sure everything works so far:


``` javascript
  Show.View = Marionette.ItemView.extend({
    template: '<div>Header!</div>'
  });
```

Now if we reload `localhost:3000` in the browser, we should see the new `Header!` markup loaded inside the `div#header-region`!



