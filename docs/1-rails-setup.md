# Installation & Set Up

## Installing Ruby

**Mac Users**: Ruby is installed for you already - you can move on to "Installing Rails".

**PC Users**: If you're working on a PC, you will need to install a Virtual Machine and develop on a Linux Operating System to work with Ruby/Rails. To set up a Linux VM, you can [follow this guide](#). 

## Installing Rails

The following is done using the **terminal**.

1. Firstly, inspect which version of ruby your system is using by: `ruby -v`
  * install *rbenv* if needed to manage your local ruby environment: https://github.com/sstephenson/rbenv
    - if you have homebrew installed, that's the best way to install rbenv and ruby-install, otherwise follow the instructions
  * in the root of your working folder, set the local version to 2.0.0-p247: `rbenv local 2.0.0-p247`
2. Install rails `gem install rails` or `sudo gem install rails`
3. Create a new rails app called **buildateam**
  * if you used `rbenv local`, create with `rbenv exec rails new buildateam`
  * otherwise, just use `rails new buildateam`
4. Make sure everything set up ok
  * navigate to inside the new app folder `cd buildateam`
  * run the WEBrick server `rails server`
  * go to localhost:3000 and confirm your web app is running!


Reference: http://guides.rubyonrails.org/getting_started.html  


### Optional: API Setup introduction

The following briefly covers the most basic steps in setting up your own rails-based API, just to give you an idea of what the different pieces do. If you want to move straight to the Javascript-focused App-building:
* Download the app scaffolding **[here][download_link]**,
* Then, skip to [Step 2](#).

## Setting up your API

We'll be using the default SQLite database for this tutorial.

* Create an "api" namespace in `config/routes.rb`
  
``` ruby
  namespace :api, defaults: {format: :json}  do
    # api resources will go here
  end

```
* Create your first controller inside the api namespace, for *members*:  
`rails g controller api/members --assets=false`

The `--assets=false` bit is important for us because we don't want rails to create the static js and css and view files. We just want the controller.

Alternatively, just create the controller.rb file right inside the `controllers/api` directory. Must follow the `<name>_controller.rb` convention.

``` ruby

# Note the CamelCase Namespace correlates with the name "members"
class Api::MembersController < ApplicationController

  # what will be called when we navigate to api/members
  def index
    # we'll fill this out next
  end

end

```

Because our API is as simple as possible, we're just parroting back the data from the database. To do that, we create a definiton `def` like so:

``` ruby
  # what will be called when we navigate to /api/members
  def index
    # Defines the "members" variable to the data: All Members
    @members = Member.all
    # returns a JSON object of the variable "members"
    render json: @members
  end
```

* The controller represents your first resource. Add that to `config/routes.rb` within the api namespace:
  
``` ruby
  namespace :api, defaults: {format: :json}  do
    resource :members
  end

```

If you navigate to `localhost:3000/api/members`, you'll get an error stating something like "uninitialized constant". We need to create the data model in order for it to display properly.


## Adding Models & Migrating Data

Next we'll set up the Models that will also create migrations for our database using the rails generator.

* Create a member model with this command:  
` rails g model member first_name:string last_name:string`

This line tells rails we want a `Member` model with 2 fields: `first_name`, and `last_name`, and the data type for both of them is a `string`.

Rails will automatically create a migration for us in addition to the model.

*__Note:__ You may have noticed that we called the controller "members", while the model was called "member". That's is quite intentional and the best practice for naming resources. The model is always singular, and the controller is plural.*


* Run the migration to add the new fields to our database with this command:  
` rake db:migrate`

Now if you navigate to `localhost:3000/api/members`, you should see an empty list, like this:
``` json
[]
```

Let's now create some sample data for us to read, which will make it easier to build out the other CRUD (Create, Read, Update, Delete) functionality in our Backbone App later.

* Add this code to `db/seeds.rb`:
``` ruby
  Member.create([
    { first_name: "Person", last_name: "One" } 
  ])
```

* Now run the following command:  
`rake db:seed`

* Navigate to `localhost:300/api/member` and you should see your new RESTful API in action!

``` json
[
  {
    "id": 1,
    "first_name": "Person",
    "last_name": "One",
    "created_at": "2014-03-23T02:31:34.350Z",
    "updated_at": "2014-03-23T02:31:34.350Z"
  }
]

```

Note that there's a few fields Rails added for us that we didn't need to worry about, like `id`, `created_at`, and `updated_at`. Very handy!

## Setting up the base HTML Page

Since this is a single page app, we want any url request not pointed at an api route to always go to the same page.

* Create a new controller under the 'site' namespace:  
`rails g controller site/home --assets=false`

* Add the following to `config/routes.rb`:
``` ruby
  # module => 'site' tells rails to look for a folder inside views called 'site'
  # and anything inside this block will live inside that 'site' folder
  scope :module => 'site', defaults: {format: :html} do
    # our default route is pointing to a view 'home#index'
    # which tells rails to look for a file 'index.html' inside a folder 'home' (within 'site')
    root :to =>'home#index'
    # anything else also goes here unless specifically set in our routes (aka, a "splat")
    get '*path' => 'home#index'
  end

```

* Two new folders will have been created inside `app/views`, the second created inside the first, like so: `site/home`
* Create the following html file inside the `home` folder: 
``` html
    <h1>Homepage!</h1>
```

If you're wondering why we don't include the rest of the html (like `head` and `body`), that's because this is actually a __partial__ - the "base" html file is located at `app/views/layouts/application.html.erb`.

* Add the following to `app/controllers/site/home_controller.rb`

``` ruby
  def index
      # return a 404 if the format is anything except 'html'
     head :not_found unless request.format == 'text/html'
  end
```

You should now be able to navigate to any url path that's not under `/api` and see this newly created homepage. This will also allow us to use Backbone url routes that look like "real" routes, instead of prefixing them with hashes `#` so as to not trigger the typical browser-reload behavior.

### The Rest of the Back-End

While these steps were important, they're only the beginning in setting up a rails back-end, and doing from start-to-finish is beyond the scope of this lesson. We've assembled all you need to build your Backbone App so you can not worry about the inner workings of the API this time around.

* Download the app scaffolding **[here][download_link]**,


#### Next: Setting up Static Files (HTML/CSS/JavaScript)


[download_link]: #
  


