# The App's Foundations

We now have set up all of the css & js libraries we need to start building, in addition to having a back-end api ready for us to connect to. So let's start building an app!

The first thing in setting up the application will be to build the **Base Module**, which will be the main "wrapper" around our app which will also serve as it's namespace (we'll just call the *app*).

## Building app.js

* Create a folder inside `assets/javascripts` called `backbone`. We'll put all of our application code in here.
* Create a new javascript file: `app.js`

*sidenote*: If this wasn't inside of a rails app, we could call it `application.js`, but that's a special file inside rails and messes things up. So we call it `app.js` instead.

* Add the following to the new `app.js` file:

``` javascript
// best practice, wrap your scripts in an immediate
// function to control scope
(function() {

  // define this.App in the global namespace
  this.App = (function(Backbone, Marionette) {
    
    // creates a new Marionette Application instance
    var App = new Marionette.Application();

    // will run when we call App.start();
    App.addInitializer(function() {
      console.log('App started!');
    });

    return App;
  
  })(Backbone, Marionette);

}).call(this);

```

* Next, open the `assets/javascripts/application.js` file and add the following immeditely below the other lines of '//= require`:

``` javascript
//= require backbone/app

$(function() {  
  App.start();
});

```

We're adding the new `backbone/app.js` file to our list of includes, and also created a jquery-based initializer function that's going to call `App.start()` once the DOM is ready.

* Open up the browser to `http://localhost:3000` and open up the web inspector.

* You should see "App started" in the console from our code inside `App.addInitializer`!

## Set up the Event Messenger

This important part of the app is how we'll communicate with the various modules within the app in addition to responding to where our users navigate within the browser with another component called "Routes", which we'll set up soon.

Adjust `backbone/app.js` to look like this:

``` javascript
(function() {

  // define this.App in the global namespace
  this.App = (function(Backbone, Marionette) {
    
    // creates a new Marionette Application instance
    var App = new Marionette.Application();

    // Creates a messaging system within the app.
    App.reqres = new Backbone.Wreqr.RequestResponse();

    // will run when we call App.start();
    App.addInitializer(function() {
      console.log('App started!');
    });

    return App;
  
  })(Backbone, Marionette);

}).call(this);

```

Let's write a sample event handler and reciever to see this great tool in action.

Inside of `App.addInitializer, add the following code:

``` javascript

    // ... other app code omitted
    App.addInitializer(function() {
      console.log('App started');

      App.reqres.setHandler('test:request', function() {
        return "Request received, here's some data!";
      });

    });
```

Now, refresh the app in your browser, open up the web inspector, and let's call this handler in the console:

```
> App.request('test:request');
  "Request received, here's some data!"
```

## Add the Base Region

Something you're going to see a whole lot of as we build this app is region management. **Regions** are how we tell Backbone & Marionette where to place our Modules (and by extension, their View componenets). But, to kick things off, we need  a "base" region, which holds the entirety of our app.

Let's also create a `#header-region` for the site header, since that will be visible everyone in the app and doesn't change very much.

* Open up the homepage view at: `assets/views/site/home/index.html`

* Replace the "Homepage!" header with a div#main-region and div#header-region:
``` html
  <div id="header-region"></div>
  <div id="main-region"></div>
```

* Now, in `app.js`, add the following above the `App.addInitializer` code:

``` javascript

  // .. other code omitted

  App.addRegions({
    headerRegion: '#header-region'
    mainRegion: '#main-region'
  });

  App.addInitializer(function() {

  // .. initializer code
```

Note that the string value to define what region is what is exactly like a jQuery selector.

Id's are not required here, it's just recommended for these top-level containers that should not have more than one on a page pretty much ever. We'll use class names for some of the lower-tier regions for sure.

## Next: Building the first Module

## Stub: Routing & History, and Initialize:After

``` javascript
  App.startHistory({pushState: true});
```
