# Static Files Setup

Now that we have the foundation for our back end working, and a base HTML page up and running, we can start to plug in our CSS and JavaScript dependencies.

We'll be using Twitter's Bootstrap for our UI framework. 

* Download the "compiled & minified" versions here: http://getbootstrap.com/getting-started/#download
* Create a `lib` directory inside of both the `assets/stylesheets` and `assets/javascripts` folders, both located inside the `app` folder.
* From the Bootstrap zip file, copy the `bootstrap.min.css` file into `assets/stylesheets/lib`, and copy the `bootstrap.min.js` file into `assets/javascripts/lib`.

* Make a copy of the Bootstrap's `fonts` folder in the `assets` folder.

If you refresh the homepage at `localhost:8000`, you should see the styles take effect. That's because rails automatically is loading everything inside the `stylesheets` and `javascripts` folder by default. 

## Javascript Libraries Setup

jQuery 2.0.3 should be loaded by Rails, so there's no need to download it. 

* Download Marionette's "pre-packaged" zip file, which will include Backbone & Underscore as well as a few other dependencies: http://marionettejs.com/#download

* Create a `lib` folder inside `assets/javascripts`.

* Copy `backbone.babysitter.js`, `backbone.js`, `backbone.marionette.js`, `backbone.wreqr.js`, `json2.js` and `underscore.js` into the `assets/javascripts/lib` folder. (skip jQuery as mentioned)

* Now we want to make some minor changes to the `javascripts/application.js` file to ensure some of our dependencies are loaded in the order we want them to.

Replace:

``` javascript
//= require_tree .
```

With:

``` javascript
//= require lib/json2
//= require lib/underscore
//= require lib/backbone
//= require lib/backbone.babysitter
//= require lib/backbone.wreqr
//= require lib/backbone.marionette
//= require lib/bootstrap.min
```

This tells Rails to not just load everything inside the `javascripts` folder however it wants, but instead load these files in this specific order, and ignore everything else. 

We'll add to this file as we build out our application, deliberately including only the files we need and in the order we require.

We now have everything we need to start building our app!

### Next: Anatomy of a Backbone App