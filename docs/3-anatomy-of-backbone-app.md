# Anatomy of a Backbone App

![App Anatomy](images/app-anatomy.png "App Anatomy")

There are 5 main 'pieces' to an app:

1. **Routes** - This refers to the URL endpoints where we retrieve data from the API
2. **Entities** - Retrieves and saves data (all of the CRUD actions sent to the back-end)
3. **Modules** - Determines which Controllers to invoke based on the URL path or event trigger called.
4. **Controllers** - Connect entities to views and determine which to show based on the URL that is visited
5. **Views** - Takes Entity data and renders HTML, and reacts to events on the DOM 

This is very high level, and will hopefully make more sense as we break it down further and actually build out some examples.

## Making lots of little Apps, not one Big one

While we only have one main "Application", we'll be building things out in smaller __Modules__, which are in a sense "mini-apps" within our app. They each serve a specific purpose, have their own Controllers and Views, and are standalone - not directly tied in with any other part of the app, but instead __invoked__ when needed.

Each Module contains one **module.js** file, and usually at least one **controller.js** and **view.js** file.

## How the pieces fit together

![Module Request Flow](images/module-flow.png "Module Request Flow")
