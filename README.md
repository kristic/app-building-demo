# App Building Demo
This is meant to be a step-by-step introduction into building a backbone/marionette app with a real restful backend.

## Completed:
- Basic Restful Backend
- The beginnings of Backbone setup 


# TODO Outline:

- Course Outline
	* What we're going to build (demo finished product)
	* What you'll learn

- Prerequisites
	* Intermediate Javascript Knowledge
	* Very comfortable with HTML, CSS, and manipulating the dom with jQuery
	* Capable using the command line/terminal
	* Familiarity with the JSON data format 

- Deciding what to build
	* outlining MVP features
	* creating a "map" with simple wireframes

- Backbone Overview
	* Backbone's Pieces
	* Why Backbone?

- Marionette Overview
	* Marionette's Pieces
	* Why Marionette?

- Planning the Architecture
	* Don't build a big app, build lots of small ones
	* List out Modules to build based on concepts
	* Mock up JSON example data on a given page

- Anatomy of an App
	* Each module is a mini-app inside the larger application
	* Module-level pieces
	* App-wide shared pieces

- Set Up the Scaffolding
	* Extending Marionette/Backbone Base Modules & Why
	* Set up Folder structure
	* Main JS and base App.js files
	* Routes & Entities
	* Views
	* Set up Testing environment

- Set up the Back end
	* Simple Rails to serve a JSON API

- The first SubModule
	* end-to-end first build
	* refactor
	* writing tests

- Build out & connect each Module
	* communicating between modules

- Adding new features to existing Modules

- Wrap Up

